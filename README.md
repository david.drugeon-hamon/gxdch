# Gaia-X Digital Clearing House

This document contains technical guidelines for the service providers aiming at operating a Gaia-X Digital Clearing House.

[[_TOC_]]

## Overview

The [Gaia-X Digital Clearing House (GXDGH)](https://gaia-x.eu/gxdch/) is the mechanism through which Gaia-X is operationalized in the market.
The [Gaia-X Framework](http://docs.gaia-x.eu/framework/) contains functional specifications, technical requirements, and the software to use in order to become Gaia-X compliant and/or Gaia-X compatible.
The GXDCH contains a subset of the software components in the Gaia-X Framework: the components that are mandatory and some of the optional ones.  

The Gaia-X Association reuses open-source components or develops new open-source ones that go into the GXDCH.

A GXDCH instance runs the engine to validate the Gaia-X rules, therefore becoming the go-to place to become Gaia-X compliant.
The instances are non-exclusive, interchangeable, and operated by multiple market operators.  

This document is targetting the release codename _TAGUS_ which is based on simple replica of the same software deployment by various cloud service providers.

In _TAGUS_, those replica are kept in sync using the same versioning release and regular update.
There is no network synchronisation between the various instances yet. This is targetted for the next release codename _LOIRE_.

Each GXDCH instance must be operated by a service provider according to rules defined with and approved by the Gaia-X AISBL.
Such providers have then the role of [Federator](https://docs.gaia-x.eu/technical-committee/architecture-document/22.10/conceptual_model/#federator).
The Gaia-X AISBL is not an operator itself.
Any operator compliant to the requirements defined by the Gaia-X AISBL and featuring the necessary characteristics as defined by the Gaia-X AISBL can become a GXDCH federator.

## Description of the GXDCH components
The components included in the GXDCH can vary from one release to another, as more
services become available with time. This is the current list of components:

### Mandatory components
* Gaia-X Registry: this service implements the 
[Gaia-X Registry](https://gaia-x.gitlab.io/technical-committee/architecture-document/operating_model/#gaia-x-registry)
as described in the Architecture Document. It is the backbone of the ecosystem
governance as it stores the Trust Anchors accepted by the Gaia-X Trust Framework,
the shapes and schemas used to validate the Gaia-X compliance as well as other
relevant information, like the Terms and Conditions for users of the compliance
service..

* Gaia-X Compliance: it validates the shape and content of the Gaia-X Credentials,
and signs ones that are valid. 

* Gaia-X notarisation service for the registration number: this is a tool used to
check the validity of all registration numbers given by the participant in their
Participan Credentials.

### Optional components
* Wizard: UI to sign Verifiable Credentials in a Verifiable Presentation on
client side. Calling the Gaia-X Compliance service is integrated with the
wizard, making it possible to obtain Gaia-X Compliant Credentials by directly
using this tool.

* Wallet: Application that allows the user to store their Credentials and
present them to third parties when needed.

* Catalogue: Not available yet

### Versionning
In order to allow several instances of different version to be accessible in parallel, we specify paths during the deployment.
That means that `/development` uses the latest `development` image of a component, `/v1` uses the latest `v1` image, and `/main` uses the latest `main` image.

Please check each component for all the available images and releases.

To ensure consistency, we ask you to use the same convention for your instances.

Each component provides [several docker images](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/tree/main#images-tags). We suggest you use the latest stable images if you intend to only deploy "production-ready" instance. This way, you'll get minor and fixes update via a simple restart of your deployment.

## Installation instructions

> :warning: **This documentation still needs some improvements**

### Hardware Requirements

The Gaia-X Digital Clearing House uses a limited amount of computing resources when deployed, less than 256MB of RAM for each instance of registry, compliance and notary.

The MongoDB used by the registry requires at least 1 vCPU and 600 MB of RAM. We use an attached volume of 8Gio, but 1 or 2Gio should be sufficient as there is 2 collections, with 4000 documents.

You can make the choice to use your own Mongo Atlas cluster if you want HA. Please note that the trusted anchors are retrieved automatically at registry startup.

### Software Prerequisites

The components are built into docker images, and while it is not a hard requirement, we recommend deploying them on a Kubernetes cluster of your choice.

- `gx-registry` should be given access to the internet as it will pull SSL and eIDAS certs directly from trusted sources.

- `gaia-x-notary-registrationnumber` should be given access to the internet as it will call trusted sources to assert registration numbers.

- `gx-compliance` should be given access to `gx-registry`. Be default, the component will try to call directly inside the cluster. You can change this behavior by specifying the `REGISTRY_URL` environment variable.


#### Certificate requirements

The gx-compliance and gaia-x-notary-registrationnumber will require you to provide SSL key-pair and a certificate issued from this keypair that will be used to sign the issued credentials and allow validation. The certificate needs to be an eIDAS ECert, as specified in the Trust Framework. Please check the available options in your country on how to get such a certificate. 

- The private key should be provided in unencrypted [PKCS #8](https://en.wikipedia.org/wiki/PKCS_8) format with line feed removed.
- The certificate should be provided in unencrypted [PKCS #8](https://en.wikipedia.org/wiki/PKCS_8) format. Line feed can be kept.

> :warning: **The project does not support PKCS #11 or any else format that PKCS #8**


In the case of usage of Helm Chart, these variables should be base64 encoded.

### User Requirements
-- TODO: Add any sudo rights or specific user configuration

### Installation steps

The Gaia-X Lab provides a Helm chart for each component to ease installation on a Kubernetes cluster.
You can use it or use your own way to deploy components.
The helm chart relies on [cert-manager](https://cert-manager.io/docs/installation/helm/#installing-with-helm) to provide Let's Encrypt certificates and [nginx](https://kubernetes.github.io/ingress-nginx/deploy/#quick-start) as cluster ingress

Each component has a section explaining the variables available in the helm chart and an example to deploy a specific version with helm.

As the compliance service will try to connect to the registry, please follow the order below, first registry, then compliance and notary.

1. [Deploy the registry](https://gitlab.com/gaia-x/lab/compliance/gx-compliance/-/tree/main#deployment)

```shell
helm upgrade --install -n "v1" --create-namespace gx-registry ./k8s/gx-registry --set "nameOverride=v1,ingress.hosts[0].host=registry.yourdomain.tld,ingress.hosts[0].paths[0].path=/v1,image.tag=v1,ingress.hosts[0].paths[0].pathType=Prefix"
```

2. [Deploy the compliance](https://gitlab.com/gaia-x/lab/compliance/gx-registry/-/tree/main#deployment)

```shell
helm upgrade --install -n "v1" --create-namespace gx-compliance ./k8s/gx-compliance --set "nameOverride=v1,ingress.hosts[0].host=compliance.yourdomain.tld,ingress.hosts[0].paths[0].path=/v1,image.tag=v1,ingress.hosts[0].paths[0].pathType=Prefix,privateKey=$complianceKey,X509_CERTIFICATE=$complianceCert"
```

3. [Deploy the notary](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/-/tree/main#deployment)

```shell
helm upgrade --install -n "v1" --create-namespace gx-notary ./k8s/gx-notary --set "nameOverride=v1,ingress.hosts[0].host=registrationnumber.notary.yourdomain.tld,ingress.hosts[0].paths[0].path=/v1,image.tag=v1,ingress.hosts[0].paths[0].pathType=Prefix,privateKey=$privateKey,X509_CERTIFICATE=$x509"
```


Every component should provide an OpenAPI page after deployment under `hostname/path/docs` e.g. https://compliance.lab.gaia-x.eu/v1/docs

-- TODO: Add test on each component readme and add links here  

### Using the GXDCH

During the licensed model operationalization of Gaia-X, the association provides a load-balancer in the form of some HAProxy nodes pointing to Clearing House instances. These HAProxy nodes are managed through  [Load Balancer ansible scripts](loadbalancer/README.md). The Gaia-X CTO Team is responsible for these scripts and their update after a clearing house is deployed. You can still prepare a merge request on the [HAProxy configuration](https://gitlab.com/gaia-x/lab/gxdch/-/blob/main/loadbalancer/haproxy.cfg) to include your GXDCH instance.

The services are accessible behind three subdomains:

- https://registrationnumber.notary.gaia-x.eu/v1/docs/
- https://compliance.gaia-x.eu/v1/docs/
- https://registry.gaia-x.eu/v1/docs/
